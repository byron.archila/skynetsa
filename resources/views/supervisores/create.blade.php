@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

<div class="container-fluid">
    <div class="card shadow mb-12">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="m-0 font-weight-bold text-primary">Agregar nueva visita</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Técnico asignado</label>
                        <select class="form-control required" name="tecnico" id="tecnico">
                            <option value="">Seleccione el técnico...</option>
                            @foreach ($tecnicos as $tec)
                                <option value="{{$tec['empleado_plaza_id']}}">{{$tec['full_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="">Cliente</label>
                        <select class="form-control required" name="cliente" id="cliente">
                            <option value="">Seleccione el cliente...</option>
                            @foreach ($clientes as $cliente)
                                <option value="{{$cliente['id']}}">{{$cliente['nombre']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="">Fecha Programada</label>
                        <input type="date" class="form-control required" id="fecha" name="fecha">
                    </div>
                    <div class="col-md-8">
                        <label for="">Descripción</label>
                        <textarea type="text" class="form-control required" id="descripcion" name="descripcion"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <button onclick="guardarVisita()" style="text-align:right" class="btn btn-success">Guardar</button>
            <a href="{{url()->previous()}}" class="btn btn-danger">Cancelar</a>
            <br>
        </div>
    </div>
</div>

<!--FIN DEL CONTENIDO PRINCIPAL-->
@include('layouts.parte_inferior')

<script>
    function guardarVisita(){
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let tecnico = $('#tecnico').val();
            let cliente = $('#cliente').val();
            let descripcion = $('#descripcion').val();
            let fecha = $('#fecha').val();

            $.ajax({
                url: "{{route('guardarVisita')}}",
                type: "post",
                data: {
                    tecnico : tecnico,
                    cliente : cliente,
                    descripcion : descripcion,
                    fecha : fecha,
                    _token : "{{csrf_token()}}"
                }
            }).done(function(res){
                if(res == 'success'){
                    swal({
                        title: "Success",
                        text: "Visita guardada exitosamente.",
                        type: "success",
                        showConfirmButton: true
                        },
                        function(isConfirm){
                            window.location.href = "{{ route('supervisores')}}";
                        }
                    );


                }else{
                    swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                }
            })
        }
    }
</script>
