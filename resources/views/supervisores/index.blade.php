@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

    <div class="container-fluid">
        <div class="card shadow mb-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="m-0 font-weight-bold text-primary">Visitas Planificadas</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="input-group mb-3" >
                        <div class="float-right">
                            <a style="margin-left: 12px" href="{{route('createVisita')}}" type="submit" class="btn btn-success">[+] Panificar Visita</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table id="tablaVisitas" class="table table-bordered table-condensed table-striped" width="100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>Técnico asignado</th>
                                    <th>Nombre Cliente</th>
                                    <th>Fecha Programada</th>
                                    <th>Estado</th>
                                    <th>Descripción</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editarVisitaModal" tabindex="-1" role="dialog"
        aria-labelledby="editarVisitaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="editarVisita_content">
            </div>
        </div>
    </div>

    <!--FIN DEL CONTENIDO PRINCIPAL-->
    @include('layouts.parte_inferior')

<script>
    $(document).ready(function() {
        tablaVisitas();
    });

    function tablaVisitas() {
        $('#tablaVisitas').DataTable({
            autoWidth: true,
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            scrollCollapse: false,
            scrollX: true,

            ajax: {
                url: "{{ route('visitasTabla') }}",
                type: 'get'
            },
            aoColumns: [
                {
                    data: 'full_name'
                },
                {
                    data: 'nombre'
                },
                {
                    data: 'fecha_programada'
                },
                {
                    data: null,
                    orderable: false,

                    render: function(data, type, row, meta) {
                        if(row.estado_id == 1)
                        {
                            var output = "<span style='color: white' class='badge rounded-pill bg-info'>Agendada</span>";
                        }else if(row.estado_id == 2){
                            var output = "<span style='color: white' class='badge rounded-pill bg-warning'>En progreso</span>";
                        }else{
                            var output = "<span style='color: white' class='badge rounded-pill bg-success'>Completada</span>";
                        }

                        return output;
                    }
                },
                {
                    data: 'descripcion'
                },
                {
                    data: 'actions',
                    orderable: false,

                    render: function(data, type, row, meta) {
                        var output = "<button title='Ver visita' class='badge rounded-pill bg-info'" +
                                "onclick='ViewVisita(" + row.id + ")'>" +
                                "<i class='fas fa-eye'></i>" +
                                "</button>" +
                            "<button class='badge rounded-pill bg-info'" +
                            "onclick='editarVisita(" + row.id + ")'>" +
                            "<i class='fas fa-edit'></i>" +
                            "</button>" +
                            "<button class='badge rounded-pill bg-danger'" +
                            " onclick='eliminarVisita(" + row.id + ")'>" +
                            "<i class='fas fa-trash-alt'></i>" +
                            "</button>";

                        return output;
                    }
                }
            ]
        });
    }

    function eliminarVisita(id)
    {
        swal({
            title: "¿Deseas eliminar esta visita al cliente?",
            text: "No estará disponible en el sistema",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Eliminar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false },

            function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: "{{route('deleteVisita')}}",
                    type: "post",
                    data: {
                        id: id,
                        _token : "{{csrf_token()}}"
                    }
                }).done(function(res){
                    if(res == 'success'){
                        swal({
                            title: "Success",
                            text: "Visita eliminada exitosamente.",
                            type: "success",
                            showConfirmButton: true
                        });

                        $('#tablaVisitas').DataTable().ajax.reload();
                    }else{
                        swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                    }
                });
            } else {
            swal("¡Cancelado!",
            "No se elimino la visita",
            "error");
            }
        });
    }

    function editarVisita(id)
    {
        var editarVisitaModal = $('#editarVisitaModal');
        var editarVisita_content = $('#editarVisita_content');

        $.ajax({
            url: "{{ route('verVisita') }}",
            type: 'post',
            data: {
                id: id,
                _token: "{{ csrf_token() }}"
            }
        }).done(function(data) {
            editarVisitaModal.modal('toggle');
            editarVisita_content.html(data);
        })
    }

    function ViewVisita(id)
        {
            var viewVisitaModal = $('#editarVisitaModal');
            var viewVisita_content = $('#editarVisita_content');

            $.ajax({
                url: "{{ route('verVisitaSupervisor') }}",
                type: 'post',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}"
                }
            }).done(function(data) {
                viewVisitaModal.modal('toggle');
                viewVisita_content.html(data);
            })
        }
</script>
