<div class="modal-header">
    <h5 class="modal-title" id="guardarProductoModalLabel">Visita agendada</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-5">
            <label for="">Cliente</label>
            <input type="text" value="{{ $visita['nombre_cliente'] }}" class="form-control required"
                id="cliente" name="cliente" disabled>
        </div>
        <div class="col-md-5">
            <label for="">Fecha Programada</label>
            <input type="date" value="{{ $visita['fecha_programada'] }}" class="form-control required"
                id="fecha" name="fecha" disabled>
        </div>
        <div class="col-md-8">
            <label for="">Descripción</label>
            <textarea type="text" class="form-control required" id="descripcion" name="descripcion" disabled>{{ $visita['descripcion'] }}</textarea>
        </div>
    </div>
    <br>
    <hr>
    <div class="row">
        <h3>Logs de la visita</h3>
        <div class="col-md-12">
            <table id="logsTabla" class="table table-bordered table-condensed table-striped" width="100%">
                <thead class="bg-primary text-white">
                    <th>Comentario</th>
                    <th>fecha</th>
                    <th>Estatus</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <br>
</div>

<script>
    $(document).ready(function() {
        logsTabla();
    });

    function logsTabla() {
        $('#logsTabla').DataTable({
            autoWidth: true,
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            scrollCollapse: false,
            scrollX: true,

            ajax: {
                url: "{{ route('logsTabla') }}",
                type: 'get',
                data: {
                    id: {{ $visita['id'] }}
                }
            },
            order: [[1, 'asc']],
            aoColumns: [
                {
                    data: 'comentario'
                },
                {
                    data: 'date'
                },
                {
                    data: null,
                    orderable: false,

                    render: function(data, type, row, meta) {
                        if(row.estado == 1)
                        {
                            var output = "<span style='color: white' class='badge rounded-pill bg-info'>Agendada</span>";
                        }else if(row.estado == 2){
                            var output = "<span style='color: white' class='badge rounded-pill bg-warning'>En progreso</span>";
                        }else{
                            var output = "<span style='color: white' class='badge rounded-pill bg-success'>Completada</span>";
                        }

                        return output;
                    }
                }
            ]
        });
    }
</script>
