<div class="modal-header">
    <h5 class="modal-title" id="guardarProductoModalLabel">Editar Visita</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="row">
            <div class="col-md-4">
                <input type="hidden" value="{{$visita['id']}}" name="id" id="id">
                <label for="">Técnico asignado</label>
                <select class="form-control required" name="tecnico" id="tecnico">
                    <option value="">Seleccione el técnico...</option>
                    @foreach ($tecnicos as $tec)
                        <option value="{{$tec['empleado_plaza_id']}}" {{$visita['empleado_plaza_id'] == $tec['empleado_plaza_id'] ? 'Selected' : ''}}>{{$tec['full_name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="">Cliente</label>
                <select class="form-control required" name="cliente" id="cliente" disabled>
                    @foreach ($clientes as $cliente)
                        <option value="{{$cliente['id']}}" {{$visita['cliente_id'] == $cliente['id'] ? 'Selected' : ''}}>{{$cliente['nombre']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="">Fecha Programada</label>
                <input type="date" value="{{$visita['fecha_programada']}}" class="form-control required" id="fecha" name="fecha">
            </div>
            <div class="col-md-8">
                <label for="">Descripción</label>
                <textarea type="text" class="form-control required" id="descripcion" name="descripcion">{{$visita['descripcion']}}</textarea>
            </div>
        </div>
    </div>
    <br>
    <div class="modal-footer">
        <button onclick="actualizar()" style="text-align:right" class="btn btn-success">Guardar</button>
    </div>
</div>

<script>

   function actualizar()
   {
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let id = $('#id').val();
            let tecnico = $('#tecnico').val();
            let cliente = $('#cliente').val();
            let descripcion = $('#descripcion').val();
            let fecha = $('#fecha').val();

            swal({
            title: "¿Deseas actualizar la información de la visita?",
            text: "se guardarán todos los cambios realizados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Actualizar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false },

            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{route('actualizarVisita')}}",
                        type: "post",
                        data: {
                            id: id,
                            tecnico: tecnico,
                            descripcion: descripcion,
                            fecha: fecha,
                            _token : "{{csrf_token()}}"
                        }
                    }).done(function(res){
                        if(res == 'success'){
                            swal({
                                title: "Success",
                                text: "Visita actualizada exitosamente!",
                                type: "success",
                                showConfirmButton: true
                            });

                            $('#editarVisitaModal').modal('hide');
                            $('#tablaVisitas').DataTable().ajax.reload();
                        }else{
                            swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                        }
                    });
                } else {
                    swal("¡Cancelado!",
                    "No se ha actualizado la visita al cliente!",
                    "error");
                }
            });
        }
   }

</script>
