@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-secondary text-white">Ingresar al Sistema
                </div>
                <div class="card-body bg-gradient-primary text-white">
                    <form class="user" method="POST" action="{{ route('loginAuth') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Usuario</label>
                            <input type="email" class="form-control form-control-user {{ $errors->has('usuario') ? 'has-error' : '' }}"
                                id="usuario" name="usuario" aria-describedby="usuarioHelp"
                                placeholder="ejemplo@correo.com..." value="{{ old('usuario') }}">
                                {!!  $errors->first('usuario',  '<span class="bg-danger help-block">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control form-control-user {{ $errors->has('password') ? 'has-error' : '' }}"
                                id="password" name="password" placeholder="Contraseña...">
                                {!!  $errors->first('password',  '<span class="bg-danger help-block">:message</span>') !!}
                        </div>
                        <button class="btn btn-warning btn-user btn-block">LOGIN</button>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
