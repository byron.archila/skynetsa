@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

<div class="container-fluid">
    <div class="card shadow mb-12">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="m-0 font-weight-bold text-primary">Agregar nuevo cliente</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Nombre</label>
                        <input type="text" class="form-control required" id="nombre" name="nombre">
                    </div>
                    <div class="col-md-4">
                        <label for="">Correo electronico</label>
                        <input type="email" class="form-control required" id="email" name="email" onchange="validateEmail()">
                    </div>
                    <div class="col-md-4">
                        <label for="">Telefono (opcional)</label>
                        <input type="number" class="form-control" id="telefono" name="telefono">
                    </div>
                    <div class="col-md-4">
                        <label for="">Dirección</label>
                        <input type="text" class="form-control required" id="direccion" name="direccion">
                    </div>
                    <div class="col-md-4">
                        <label for="">Latitud</label>
                        <input type="text" class="form-control required" id="latitud" name="latitud" disabled>
                    </div>
                    <div class="col-md-4">
                        <label for="">Longitud</label>
                        <input type="text" class="form-control required" id="longitud" name="longitud" disabled>
                    </div>
                    <div class="col-md-6">
                        <label for="">Descripción</label>
                        <textarea type="text" class="form-control required" id="descripcion" name="descripcion"></textarea>
                    </div>
                    <div class="col-md-6">
                        <button style="margin-top: 50px; margin-left: 100px" onclick="guardarCliente()" class="btn btn-success">Guardar</button>
                        <a style="margin-top: 50px; margin-left: 20px" href="{{url()->previous()}}" class="btn btn-danger">Cancelar</a>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <div id="mapa" style="width: 100%; height: 500px">
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<!--FIN DEL CONTENIDO PRINCIPAL-->
@include('layouts.parte_inferior')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9SPPRlHC64nqDEZkwuqi_EDcZ8lLGyvI&callback=iniciarMap" async defer></script>
<script>
    function guardarCliente(){
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        let valido = validateEmail();
        if(!valido){
            return false;
        }

        if($('#latitud').val() == null || $('#latitud').val() == ""){
            swal("Upss!", "Selecciona un punto en el mapa para obtener la latitud y longitud", "warning");
            return false;
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let nombre = $('#nombre').val();
            let email = $('#email').val();
            let telefono = $('#telefono').val();
            let direccion = $('#direccion').val();
            let latitud = $('#latitud').val();
            let longitud = $('#longitud').val();
            let descripcion = $('#descripcion').val();

            $.ajax({
                url: "{{route('storeCliente')}}",
                type: "post",
                data: {
                    nombre: nombre,
                    email: email,
                    telefono: telefono,
                    direccion: direccion,
                    latitud: latitud,
                    longitud: longitud,
                    descripcion: descripcion,
                    _token : "{{csrf_token()}}"
                }
            }).done(function(res){
                if(res == 'success'){
                    swal({
                        title: "Success",
                        text: "Cliente guardado exitosamente.",
                        type: "success",
                        showConfirmButton: true
                        },
                        function(isConfirm){
                            window.location.href = "{{ route('clientes')}}";
                        }
                    );


                }else{
                    swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                }
            })
        }
    }

    function validateEmail(){
        // Get our input reference.
        var emailField = document.getElementById('email');
        // Define our regular expression.
        var validEmail =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
        // Using test we can check if the text match the pattern
        if( validEmail.test(emailField.value) ){
            return true;
        }else{
            swal("Upss!", "El formato del Correo es inválido", "error");
            return false;
        }
    }

    function iniciarMap()
    {
        let latitud = 14.642313027619839;
        let longitud = -90.51327721365053;

        coordenadas = {
            lng : longitud,
            lat : latitud
        }

        generarMapa(coordenadas);
    }

    function generarMapa(coordenadas)
    {
        //Iniciar el mapa centrado en la posicion inicial creada
        let mapa = new google.maps.Map(document.getElementById('mapa'),{
            zoom: 12,
            center: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        //Añadir marcador en el mapa en la posicion inicial
        marcador = new google.maps.Marker({
            map: mapa,
            draggable: true,
            position: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        // obtener la latitud y longitud en la posicion del marcador que creamos
        marcador.addListener('dragend', function(event){
            document.getElementById('latitud').value = this.getPosition().lat();
            document.getElementById('longitud').value = this.getPosition().lng();
        })
    }
</script>
