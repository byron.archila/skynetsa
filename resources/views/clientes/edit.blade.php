<div class="modal-header">
    <h5 class="modal-title" id="guardarProductoModalLabel">Editar cliente: {{$cliente['nombre']}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="row">
            <div class="col-md-4">
                <input type="hidden" value="{{$cliente['id']}}" name="id" id="id">
                <label for="">Nombre</label>
                <input type="text" class="form-control required" value="{{$cliente['nombre']}}" id="nombre" name="nombre">
            </div>
            <div class="col-md-4">
                <label for="">Correo electronico</label>
                <input type="email" class="form-control required" value="{{$cliente['email']}}" id="email" name="email" onchange="validateEmail()">
            </div>
            <div class="col-md-4">
                <label for="">Estatus</label>
                <select class="form-control" name="estado" id="estado">
                    <option value="1" {{$cliente['activo'] == 1 ? 'Selected' : ''}}>Activo</option>
                    <option value="0" {{$cliente['activo'] == 0 ? 'Selected' : ''}}>Inactivo</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="">Telefono (opcional)</label>
                <input type="number" class="form-control" value="{{$cliente['telefono']}}" id="telefono" name="telefono">
            </div>
            <div class="col-md-4">
                <label for="">Dirección</label>
                <input type="text" class="form-control required" value="{{$cliente['direccion']}}" id="direccion" name="direccion">
            </div>
            <div class="col-md-4">
                <label for="">Latitud</label>
                <input type="text" class="form-control required" value="{{$cliente['latitud']}}" id="latitud" name="latitud" disabled>
            </div>
            <div class="col-md-8">
                <label for="">Descripción</label>
                <input type="text" class="form-control required" value="{{$cliente['descripcion']}}" id="descripcion" name="descripcion">
            </div>
            <div class="col-md-4">
                <label for="">Longitud</label>
                <input type="text" class="form-control required" value="{{$cliente['longitud']}}" id="longitud" name="longitud" disabled>
            </div>
        </div>
    </div>
    <br>
    <div class="modal-footer">
        <button onclick="actualizar()" style="text-align:right" class="btn btn-success">Actualizar</button>
    </div>
    <br>
    <div class="col-md-12">
        <div id="mapa" style="width: 100%; height: 500px">
        </div>
    </div>
    <br>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9SPPRlHC64nqDEZkwuqi_EDcZ8lLGyvI&callback=iniciarMapa" async defer></script>
<script>

    function actualizar()
   {
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        let valido = validateEmail();
        if(!valido){
            return false;
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let id = $('#id').val();
            let nombre = $('#nombre').val();
            let email = $('#email').val();
            let descripcion = $('#descripcion').val();
            let direccion = $('#direccion').val();
            let estado = $('#estado').val();
            let telefono = $('#telefono').val();
            let latitud = $('#latitud').val();
            let longitud = $('#longitud').val();

            swal({
            title: "¿Deseas actualizar la información del Cliente?",
            text: "se guardarán todos los cambios realizados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Actualizar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false },

            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{route('actualizarCliente')}}",
                        type: "post",
                        data: {
                            id: id,
                            nombre: nombre,
                            email: email,
                            descripcion: descripcion,
                            direccion: direccion,
                            telefono: telefono,
                            estado: estado,
                            latitud: latitud,
                            longitud: longitud,
                            _token : "{{csrf_token()}}"
                        }
                    }).done(function(res){
                        if(res == 'success'){
                            swal({
                                title: "Success",
                                text: "Cliente actualizado exitosamente!",
                                type: "success",
                                showConfirmButton: true
                            });

                            $('#editarClienteModal').modal('hide');
                            $('#tablaClientes').DataTable().ajax.reload();
                        }else{
                            swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                        }
                    });
                } else {
                    swal("¡Cancelado!",
                    "No se ha actualizado la información del cliente!",
                    "error");
                }
            });
        }
   }

   function validateEmail(){
    // Get our input reference.
    var emailField = document.getElementById('email');

    // Define our regular expression.
    var validEmail =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;

    // Using test we can check if the text match the pattern
    if( validEmail.test(emailField.value) ){
        return true;
    }else{
        swal("Upss!", "El formato del Correo es inválido", "error");
        return false;
    }
    }

    function iniciarMapa()
    {
        let latitud = document.getElementById('latitud').value;
        let longitud = document.getElementById('longitud').value;

        coordenadas = {
            lng : longitud,
            lat : latitud
        }

        generarMapa(coordenadas);
    }

    function generarMapa(coordenadas)
    {
        //Iniciar el mapa centrado en la posicion inicial creada
        let mapa = new google.maps.Map(document.getElementById('mapa'),{
            zoom: 18,
            center: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        //Añadir marcador en el mapa en la posicion inicial
        marcador = new google.maps.Marker({
            map: mapa,
            draggable: true,
            position: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        // obtener la latitud y longitud en la posicion del marcador que creamos
        marcador.addListener('dragend', function(event){
            document.getElementById('latitud').value = this.getPosition().lat();
            document.getElementById('longitud').value = this.getPosition().lng();
        })
    }

</script>
