@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

    <div class="container-fluid">
        <div class="card shadow mb-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="m-0 font-weight-bold text-primary">Clientes</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="input-group mb-3" >
                        <div class="float-right">
                            <a style="margin-left: 12px" href="{{route('createCliente')}}" type="submit" class="btn btn-success">[+] Nuevo</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table id="tablaClientes" class="table table-bordered table-condensed table-striped" width="100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre Cliente</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>Descripción</th>
                                    <th>Dirección</th>
                                    <th>Latitud</th>
                                    <th>Longitud</th>
                                    <th>Estatus</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editarClienteModal" tabindex="-1" role="dialog"
        aria-labelledby="editarClienteModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="editarCliente_content">
            </div>
        </div>
    </div>

    <!--FIN DEL CONTENIDO PRINCIPAL-->
    @include('layouts.parte_inferior')

<script>
    $(document).ready(function() {
        tablaClientes();
    });

    function tablaClientes() {
        $('#tablaClientes').DataTable({
            autoWidth: true,
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            scrollCollapse: false,
            scrollX: true,

            ajax: {
                url: "{{ route('clientesTabla') }}",
                type: 'get'
            },
            aoColumns: [{
                    data: 'id'
                },
                {
                    data: 'nombre'
                },
                {
                    data: 'email'
                },
                {
                    data: 'telefono'
                },
                {
                    data: 'descripcion'
                },
                {
                    data: 'direccion'
                },
                {
                    data: 'latitud'
                },
                {
                    data: 'longitud'
                },
                {
                    data: null,
                    orderable: false,

                    render: function(data, type, row, meta) {
                        if(row.activo == 1)
                        {
                            var output = "<span style='color: white' class='badge rounded-pill bg-success'>Activo</span>";
                        }else{
                            var output = "<span style='color: white' class='badge rounded-pill bg-danger'>Inactivo</span>";
                        }

                        return output;
                    }
                },
                {
                    data: 'actions',
                    orderable: false,

                    render: function(data, type, row, meta) {
                        var output = "<button class='badge rounded-pill bg-info'" +
                            "onclick='editarCliente(" + row.id + ")'>" +
                            "<i class='fas fa-edit'></i>" +
                            "</button>" +
                            "<button class='badge rounded-pill bg-danger'" +
                            " onclick='eliminarCliente(" + row.id + ")'>" +
                            "<i class='fas fa-trash-alt'></i>" +
                            "</button>";

                        return output;
                    }
                }
            ]
        });
    }

    function eliminarCliente(id)
    {
        swal({
            title: "¿Deseas eliminar este cliente?",
            text: "No estará disponible en el sistema",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Eliminar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false },

            function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: "{{route('deleteCliente')}}",
                    type: "post",
                    data: {
                        id: id,
                        _token : "{{csrf_token()}}"
                    }
                }).done(function(res){
                    if(res == 'success'){
                        swal({
                            title: "Success",
                            text: "Cliente eliminado exitosamente.",
                            type: "success",
                            showConfirmButton: true
                        });

                        $('#tablaClientes').DataTable().ajax.reload();
                    }else{
                        swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                    }
                });
            } else {
            swal("¡Cancelado!",
            "No se elimino el cliente",
            "error");
            }
        });
    }

    function editarCliente(id)
    {
        var editarClienteModal = $('#editarClienteModal');
        var editarCliente_content = $('#editarCliente_content');

        $.ajax({
            url: "{{ route('verCliente') }}",
            type: 'post',
            data: {
                id: id,
                _token: "{{ csrf_token() }}"
            }
        }).done(function(data) {
            editarClienteModal.modal('toggle');
            editarCliente_content.html(data);
        })
    }
</script>
