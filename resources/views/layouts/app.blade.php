<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('includes/css/sb-admin-2.min.css')}}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SKYNET, S.A.</title>

</head>
<body background="{{ asset('img/logo1.jpg') }}"
    style =
    "
        background-position-x:center;
        background-position-y:center;
        background-size: 125rem;
    ">
    <div class="text-center">
    <h1 class="bg-primary text-white">SKYNET, S.A.</h1>
    </div>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
