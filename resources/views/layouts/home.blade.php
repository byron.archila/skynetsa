@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

@if (session('rol') == 1 ||session('rol') == 2)
    <div class="container-fluid">
        <div class="card shadow mb-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="m-0 font-weight-bold text-primary">HOME</h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <img width="90%" height="100%" src="includes/img/home.png">
            </div>
        </div>
    </div>
@endif

@if (session('rol') == 3)
    <div class="container-fluid">
        <div class="card shadow mb-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="m-0 font-weight-bold text-primary">Visitas Asignadas</h5>
                    </div>
                </div>
            </div>
                @include('tecnicos.visitas')
        </div>
    </div>
@endif

<!--FIN DEL CONTENIDO PRINCIPAL-->
@include('layouts.parte_inferior')

@if (session('rol') == 3)
    <script>
        $(document).ready(function() {
            visitasAsignadas();
        });

        function filter()
        {
            console.log('pasa');
            visitasAsignadas();
        }

        function visitasAsignadas() {
            fecha = $('#date').val();
            $('#visitasAsignadas').DataTable({
                autoWidth: true,
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                scrollCollapse: false,
                scrollX: true,

                ajax: {
                    url: "{{ route('visitasAsignadas') }}",
                    type: 'get',
                    data:{
                        fecha: fecha
                    }
                },
                order: [[2, 'desc']],
                aoColumns: [
                    {
                        data: 'nombre'
                    },
                    {
                        data: 'direccion'
                    },
                    {
                        data: 'fecha_programada'
                    },
                    {
                        data: null,
                        orderable: false,

                        render: function(data, type, row, meta) {
                            if(row.estado_id == 1)
                            {
                                var output = "<span style='color: white' class='badge rounded-pill bg-info'>Agendada</span>";
                            }else if(row.estado_id == 2){
                                var output = "<span style='color: white' class='badge rounded-pill bg-warning'>En progreso</span>";
                            }else{
                                var output = "<span style='color: white' class='badge rounded-pill bg-success'>Completada</span>";
                            }

                            return output;
                        }
                    },
                    {
                        data: 'descripcion'
                    },
                    {
                        data: 'actions',
                        orderable: false,

                        render: function(data, type, row, meta) {
                            var output = "<button title='Ver visita' class='badge rounded-pill bg-info'" +
                                "onclick='View(" + row.id + ")'>" +
                                "<i class='fas fa-eye'></i>" +
                                "</button>" +
                                "<button title='Ver Localización' class='badge rounded-pill bg-danger'" +
                                "onclick='localizacion(" + row.id + ")'>" +
                                "<i class='fas fa-map-marker-alt'></i>" +
                                "</button>";

                            return output;
                        }
                    }
                ]
            });
        }

        function View(id)
        {
            var viewVisitaModal = $('#viewVisitaModal');
            var viewVisita_content = $('#viewVisita_content');

            $.ajax({
                url: "{{ route('verVisitaTecnico') }}",
                type: 'post',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}"
                }
            }).done(function(data) {
                viewVisitaModal.modal('toggle');
                viewVisita_content.html(data);
            })
        }

        function localizacion(id)
        {
            var viewVisitaModal = $('#viewVisitaModal');
            var viewVisita_content = $('#viewVisita_content');

            $.ajax({
                url: "{{ route('verLocalizacion') }}",
                type: 'post',
                data: {
                    visita_id: id,
                    _token: "{{ csrf_token() }}"
                }
            }).done(function(data) {
                viewVisitaModal.modal('toggle');
                viewVisita_content.html(data);
            })
        }
    </script>
@endif
