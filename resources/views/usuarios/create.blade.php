@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

<div class="container-fluid">
    <div class="card shadow mb-12">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="m-0 font-weight-bold text-primary">Agregar nuevo Usuario</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label for="">Empleado</label>
                    <select class="form-control required" name="empleado" id="empleado">
                        <option value="">Seleccione el empleado...</option>
                        @foreach ($empleados as $item)
                            <option value="{{$item['id']}}">{{$item['full_name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Rol</label>
                    <select class="form-control" name="rol" id="rol">
                        <option value="2">Supervisor</option>
                        <option value="3">Técnico</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Usuario (Correo Eléctronico)</label>
                    <input type="text" onchange="validateEmail()" class="form-control required" id="usuario" name="usuario">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <label for="">Contraseña</label>
                    <input type="password" class="form-control required" id="pass" name="pass">
                </div>
                <div class="col-md-3">
                    <label for="">Confirmar Contraseña</label>
                    <input type="password" class="form-control required" id="confirm_pass" name="confirm_pass">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button style="margin-top: 50px; margin-left: 20px" onclick="guardarUsuario()" class="btn btn-success">Guardar</button>
                    <a style="margin-top: 50px; margin-left: 20px" href="{{url()->previous()}}" class="btn btn-danger">Cancelar</a>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<!--FIN DEL CONTENIDO PRINCIPAL-->
@include('layouts.parte_inferior')

<script>
    function guardarUsuario(){
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        let valido = validateEmail();
        if(!valido){
            return false;
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let empleado = $('#empleado').val();
            let rol = $('#rol').val();
            let usuario = $('#usuario').val();
            let pass = $('#pass').val();
            let confirm_pass = $('#confirm_pass').val();

            $.ajax({
                url: "{{route('storeUsuario')}}",
                type: "post",
                data: {
                    empleado : empleado,
                    rol : rol,
                    usuario : usuario,
                    pass : pass,
                    confirm_pass : confirm_pass,
                    _token : "{{csrf_token()}}"
                }
            }).done(function(res){
                if(res == 'success'){
                    swal({
                        title: "Success",
                        text: "Usuario guardado exitosamente.",
                        type: "success",
                        showConfirmButton: true
                        },
                        function(isConfirm){
                            window.location.href = "{{ route('usuarios')}}";
                        }
                    );

                }else{
                    swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                }
            })
        }
    }

    function validateEmail(){
        // Get our input reference.
        var emailField = document.getElementById('usuario');
        // Define our regular expression.
        var validEmail =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
        // Using test we can check if the text match the pattern
        if( validEmail.test(emailField.value) ){
            return true;
        }else{
            swal("Upss!", "El formato del Correo es inválido", "error");
            return false;
        }
    }
</script>
