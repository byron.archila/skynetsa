@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

    <div class="container-fluid">
        <div class="card shadow mb-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="m-0 font-weight-bold text-primary">Empleados</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="input-group mb-3" >
                        <div class="float-right">
                            <a style="margin-left: 12px" href="{{route('createEmpleado')}}" type="submit" class="btn btn-success">[+] Nuevo</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table id="tablaEmpleados" class="table table-bordered table-condensed table-striped" width="100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre Empleado</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Plaza</th>
                                    <th>Dirección</th>
                                    <th>Zona</th>
                                    <th>Estado</th>
                                    <th>Area</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--FIN DEL CONTENIDO PRINCIPAL-->
    @include('layouts.parte_inferior')

<script>
    $(document).ready(function() {
        tablaEmpleados();
    });

    function tablaEmpleados() {
        $('#tablaEmpleados').DataTable({
            autoWidth: true,
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            scrollCollapse: false,
            scrollX: true,

            ajax: {
                url: "{{ route('empleadosTabla') }}",
                type: 'get'
            },
            aoColumns: [{
                    data: 'id'
                },
                {
                    data: 'full_name'
                },
                {
                    data: 'fech'
                },
                {
                    data: 'nombre_plaza'
                },
                {
                    data: 'direccion'
                },
                {
                    data: 'zona'
                },
                {
                    data: null,
                    orderable: false,

                    render: function(data, type, row, meta) {
                        if(row.activo == 1)
                        {
                            var output = "<span style='color: white' class='badge rounded-pill bg-success'>Activo</span>";
                        }else{
                            var output = "<span style='color: white' class='badge rounded-pill bg-danger'>Inactivo</span>";
                        }

                        return output;
                    }
                },
                {
                    data: 'nombre_area'
                }
            ]
        });
    }
</script>
