@include('layouts.parte_superior')
<!--INICIO DEL CONTENIDO PRINCIPAL-->

<div class="container-fluid">
    <div class="card shadow mb-12">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="m-0 font-weight-bold text-primary">Agregar nuevo Empleado</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="row">
                    <div class="col-md-3">
                        <label for="">Primer Nombre</label>
                        <input type="text" class="form-control required" id="primer_nombre" name="primer_nombre">
                    </div>
                    <div class="col-md-3">
                        <label for="">Segundo Nombre (opcional)</label>
                        <input type="text" class="form-control" id="segundo_nombre" name="segundo_nombre">
                    </div>
                    <div class="col-md-3">
                        <label for="">Primer Apellido</label>
                        <input type="text" class="form-control required" id="primer_apellido" name="primer_apellido">
                    </div>
                    <div class="col-md-3">
                        <label for="">Segundo Apellido (opcional)</label>
                        <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido">
                    </div>
                    <div class="col-md-3">
                        <label for="">Fecha de Nacimiento</label>
                        <input type="date" class="form-control required" id="nacimiento" name="nacimiento">
                    </div>
                    <div class="col-md-3">
                        <label for="">DPI - CUI</label>
                        <input type="number" class="form-control required" id="dpi" name="dpi">
                    </div>
                    <div class="col-md-3">
                        <label for="">Dirección</label>
                        <input type="text" class="form-control required" id="dir" name="dir">
                    </div>
                    <div class="col-md-3">
                        <label for="">Zona</label>
                        <input type="number" class="form-control required" id="zona" name="zona">
                    </div>
                    <div class="col-md-3">
                        <label for="">Municipio</label>
                        <select class="form-control" name="municipio" id="municipio">
                            @foreach ($municipios as $item)
                            <option value="{{$item['id']}}">{{$item['nombre']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <h3>Posición del empleado:</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Plaza</label>
                    <select class="form-control" name="plaza" id="plaza">
                        <option value="3">Técnico</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Área</label>
                    <select class="form-control" name="area" id="area">
                        <option value="1">Infraestructura</option>
                        <option value="2">Soporte</option>
                        <option value="3">Mantenimiento</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button style="margin-top: 50px; margin-left: 20px" onclick="guardarEmpledo()" class="btn btn-success">Guardar</button>
                    <a style="margin-top: 50px; margin-left: 20px" href="{{url()->previous()}}" class="btn btn-danger">Cancelar</a>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<!--FIN DEL CONTENIDO PRINCIPAL-->
@include('layouts.parte_inferior')

<script>
    function guardarEmpledo(){
        let req = false;
        let objs = document.getElementsByClassName("required");
        for (var i = 0; i < objs.length; i++) {
            if(objs[i].value == null || objs[i].value == ""){
                objs[i].style.backgroundColor = "#FDEDEC";
                req = true;
            }else{
                objs[i].style.backgroundColor = "";
            }
        }

        if(req){
            swal("Upss!", "Por favor completa todos los campos requeridos", "warning");
        }else{
            let primer_nombre = $('#primer_nombre').val();
            let segundo_nombre = $('#segundo_nombre').val();
            let primer_apellido = $('#primer_apellido').val();
            let segundo_apellido = $('#segundo_apellido').val();
            let nacimiento = $('#nacimiento').val();
            let dpi = $('#dpi').val();
            let dir = $('#dir').val();
            let zona = $('#zona').val();
            let plaza = $('#plaza').val();
            let area = $('#area').val();
            let municipio = $('#municipio').val();

            $.ajax({
                url: "{{route('storeEmpleado')}}",
                type: "post",
                data: {
                    primer_nombre : primer_nombre,
                    segundo_nombre : segundo_nombre,
                    primer_apellido : primer_apellido,
                    segundo_apellido : segundo_apellido,
                    nacimiento : nacimiento,
                    dpi : dpi,
                    dir : dir,
                    zona : zona,
                    area: area,
                    plaza: plaza,
                    municipio: municipio,
                    _token : "{{csrf_token()}}"
                }
            }).done(function(res){
                if(res == 'success'){
                    swal({
                        title: "Success",
                        text: "Empleado guardado exitosamente.",
                        type: "success",
                        showConfirmButton: true
                        },
                        function(isConfirm){
                            window.location.href = "{{ route('empleados')}}";
                        }
                    );


                }else{
                    swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                }
            })
        }
    }
</script>
