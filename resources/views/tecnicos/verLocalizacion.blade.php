<div class="modal-header">
    <h5 class="modal-title" id="guardarProductoModalLabel">Localización del Cliente: {{$cliente['nombre']}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="col-md-12">
        <div id="mapa" style="width: 100%; height: 500px">
        </div>
    </div>
    <br>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9SPPRlHC64nqDEZkwuqi_EDcZ8lLGyvI&callback=iniciarLocalizacion" async defer></script>
<script>
    function iniciarLocalizacion()
    {
        let latitud = {{$cliente['latitud']}};
        let longitud = {{$cliente['longitud']}};

        coordenadas = {
            lng : longitud,
            lat : latitud
        }

        generarMapa(coordenadas);
    }

    function generarMapa(coordenadas)
    {
        //Iniciar el mapa centrado en la posicion inicial creada
        let mapa = new google.maps.Map(document.getElementById('mapa'),{
            zoom: 18,
            center: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        //Añadir marcador en el mapa en la posicion inicial
        marcador = new google.maps.Marker({
            map: mapa,
            draggable: true,
            position: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
        });
        // obtener la latitud y longitud en la posicion del marcador que creamos
        marcador.addListener('dragend', function(event){
            document.getElementById('latitud').value = this.getPosition().lat();
            document.getElementById('longitud').value = this.getPosition().lng();
        })
    }

</script>
