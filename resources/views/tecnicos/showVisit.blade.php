<div class="modal-header">
    <h5 class="modal-title" id="guardarProductoModalLabel">Visita agendada</h5>
    @if ($visita['estado_id'] == 1)
        <button onclick="comenzar({{ $visita['id'] }})" style="margin-left:100px" class="btn btn-success">Comenzar
            Visita</button>
    @endif
    @if ($visita['estado_id'] == 2)
        <button onclick="finalizar({{ $visita['id'] }})" style="margin-left:100px" class="btn btn-danger">Finalizar
            Visita</button>
    @endif
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-5">
            <label for="">Cliente</label>
            <input type="text" value="{{ $visita['nombre_cliente'] }}" class="form-control required"
                id="cliente" name="cliente" disabled>
        </div>
        <div class="col-md-5">
            <label for="">Fecha Programada</label>
            <input type="date" value="{{ $visita['fecha_programada'] }}" class="form-control required"
                id="fecha" name="fecha" disabled>
        </div>
        <div class="col-md-8">
            <label for="">Descripción</label>
            <textarea type="text" class="form-control required" id="descripcion" name="descripcion" disabled>{{ $visita['descripcion'] }}</textarea>
        </div>
    </div>
    <br>
    <hr>
    <div class="row">
        @if ($visita['estado_id'] == 2)
            <div class="col-md-8">
                <label for="">comentario</label>
                <input type="text" class="form-control required" id="comentario" name="comentario" required>
            </div>
            <div class="col-md-4">
                <button onclick="guardarComentario({{ $visita['id'] }})" style="margin-top:32px"
                    class="btn btn-success">Añadir</button>
            </div>
        @endif
    </div>
    <br>
    <div class="row">
        <h3>Logs de la visita</h3>
        <div class="col-md-12">
            <table id="logsTabla" class="table table-bordered table-condensed table-striped" width="100%">
                <thead class="bg-primary text-white">
                    <th>Comentario</th>
                    <th>fecha</th>
                    <th>Estatus</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <br>
</div>

<script>
    $(document).ready(function() {
        logsTabla();
    });

    function logsTabla() {
        $('#logsTabla').DataTable({
            autoWidth: true,
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            scrollCollapse: false,
            scrollX: true,

            ajax: {
                url: "{{ route('logsTabla') }}",
                type: 'get',
                data: {
                    id: {{ $visita['id'] }}
                }
            },
            order: [[1, 'asc']],
            aoColumns: [
                {
                    data: 'comentario'
                },
                {
                    data: 'date'
                },
                {
                    data: null,
                    orderable: false,

                    render: function(data, type, row, meta) {
                        if(row.estado == 1)
                        {
                            var output = "<span style='color: white' class='badge rounded-pill bg-info'>Agendada</span>";
                        }else if(row.estado == 2){
                            var output = "<span style='color: white' class='badge rounded-pill bg-warning'>En progreso</span>";
                        }else{
                            var output = "<span style='color: white' class='badge rounded-pill bg-success'>Completada</span>";
                        }

                        return output;
                    }
                }
            ]
        });
    }

    function guardarComentario(id) {
        comentario = $('#comentario').val();

        if(comentario == null || comentario == ""){
            swal({
                title: "Error!",
                text: "Agregue el comentario para proceder",
                type: "warning",
                showConfirmButton: true
            });
            return false;
        }
        swal({
                title: "¿Desea agregar el comentario de esta visita?",
                text: "quedará registrado en los logs",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Guardar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('guardarlog') }}",
                        type: "post",
                        data: {
                            id: id,
                            comentario: comentario,
                            _token: "{{ csrf_token() }}"
                        }
                    }).done(function(res) {
                        if (res == 'success') {
                            swal({
                                title: "Success",
                                text: "Se ha guardado el comentario!",
                                type: "success",
                                showConfirmButton: true
                            });

                            $('#comentario').val('');
                            $('#logsTabla').DataTable().ajax.reload();
                        } else {
                            swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                        }
                    });
                } else {
                    swal("¡Cancelado!",
                        "",
                        "error");
                }
            }
        );
    }

    function comenzar(id) {
        swal({
                title: "¿Deseas comenzar con la visita al cliente?",
                text: "Se registrará la hora actual de entrada",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Comenzar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('comenzarVisita') }}",
                        type: "post",
                        data: {
                            id: id,
                            _token: "{{ csrf_token() }}"
                        }
                    }).done(function(res) {
                        if (res == 'success') {
                            swal({
                                title: "Success",
                                text: "La visita está en progreso!",
                                type: "success",
                                showConfirmButton: true
                            });

                            $('#viewVisitaModal').modal('hide');
                            $('#visitasAsignadas').DataTable().ajax.reload();
                        } else {
                            swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                        }
                    });
                } else {
                    swal("¡Cancelado!",
                        "No ha comenzado la visita al cliente!",
                        "error");
                }
            }
        );
    }

    function finalizar(id) {
        swal({
                title: "¿Deseas finalizar con la visita al cliente?",
                text: "Se registrará la hora actual de salida",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Finalizar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('finalizarVisita') }}",
                        type: "post",
                        data: {
                            id: id,
                            _token: "{{ csrf_token() }}"
                        }
                    }).done(function(res) {
                        if (res == 'success') {
                            swal({
                                title: "Success",
                                text: "La visita se ha finalizado correctamente!",
                                type: "success",
                                showConfirmButton: true
                            });

                            $('#viewVisitaModal').modal('hide');
                            $('#visitasAsignadas').DataTable().ajax.reload();
                        } else {
                            swal("Upss!", "Ha ocurrido un error, contacte con soporte", "error");
                        }
                    });
                } else {
                    swal("¡Cancelado!",
                        "",
                        "error");
                }
            }
        );
    }
</script>
