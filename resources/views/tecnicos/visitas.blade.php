<div class="card-body">
    <div class="row">
        <div class="col-mb-3">
            <input style="margin-left: 30px" class="form-control" type="date" value="{{$today}}"
                id="date" name="date" onchange="filter()">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="visitasAsignadas" class="table table-bordered table-condensed table-striped" width="100%">
                <thead class="bg-primary text-white">
                    <tr>
                        <th>Nombre Cliente</th>
                        <th>Dirección</th>
                        <th>Fecha Programada</th>
                        <th>Estado</th>
                        <th>Descripción</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="viewVisitaModal" tabindex="-1" role="dialog"
    aria-labelledby="viewVisitaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="viewVisita_content">
        </div>
    </div>
</div>
