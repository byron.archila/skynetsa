<?php

use App\Http\Controllers\login\AuthController;
use App\Http\Controllers\mantenimiento\ClienteController;
use App\Http\Controllers\mantenimiento\EmpleadoController;
use App\Http\Controllers\mantenimiento\SupervisorController;
use App\Http\Controllers\mantenimiento\UsuarioController;
use App\Http\Controllers\TecnicoController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

Route::get('login', function () { return view('auth.login'); } )->name('login');
Route::post('loginAuth', [AuthController::class, 'login'])->name('loginAuth');
Route::post('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('home', function () {
    if(session('usuarioAuth')){
    $today = Carbon::now()->toDateString();
        return view('layouts.home', compact('today'));
    }else{
        return redirect()->route('login');
    }
    })->name('home');

//CLIENTES
Route::get('clientes', [ClienteController::class, 'index'] )->name('clientes');
Route::get('createCliente', [ClienteController::class, 'createCliente'] )->name('createCliente');
Route::post('storeCliente', [ClienteController::class, 'storeCliente'])->name('storeCliente');
Route::get('clientesTabla', [ClienteController::class, 'clientesTabla'])->name('clientesTabla');
Route::post('deleteCliente', [ClienteController::class, 'deleteCliente'])->name('deleteCliente');
Route::post('verCliente', [ClienteController::class, 'verCliente'])->name('verCliente');
Route::post('actualizarCliente', [ClienteController::class, 'actualizarCliente'])->name('actualizarCliente');

//SUPERVISORES
Route::get('supervisores', [SupervisorController::class, 'index'] )->name('supervisores');
Route::get('createVisita', [SupervisorController::class, 'create'] )->name('createVisita');
Route::post('guardarVisita', [SupervisorController::class, 'store'])->name('guardarVisita');
Route::get('visitasTabla', [SupervisorController::class, 'visitasTabla'])->name('visitasTabla');
Route::post('deleteVisita', [SupervisorController::class, 'deleteVisita'])->name('deleteVisita');
Route::post('verVisita', [SupervisorController::class, 'verVisita'])->name('verVisita');
Route::post('actualizarVisita', [SupervisorController::class, 'actualizarVisita'])->name('actualizarVisita');
Route::post('verVisitaSupervisor', [SupervisorController::class, 'verVisitaSupervisor'])->name('verVisitaSupervisor');

//TECNICOS
Route::get('visitasAsignadas', [TecnicoController::class, 'visitasAsignadas'])->name('visitasAsignadas');
Route::post('verVisitaTecnico', [TecnicoController::class, 'verVisitaTecnico'])->name('verVisitaTecnico');
Route::post('comenzarVisita', [TecnicoController::class, 'comenzarVisita'])->name('comenzarVisita');
Route::post('finalizarVisita', [TecnicoController::class, 'finalizarVisita'])->name('finalizarVisita');
Route::post('guardarlog', [TecnicoController::class, 'guardarlog'])->name('guardarlog');
Route::get('logsTabla', [TecnicoController::class, 'logsTabla'])->name('logsTabla');
Route::post('verLocalizacion', [TecnicoController::class, 'verLocalizacion'])->name('verLocalizacion');

//EMPLEADOS
Route::get('empleados', [EmpleadoController::class, 'index'] )->name('empleados');
Route::get('create', [EmpleadoController::class, 'createEmpleado'] )->name('createEmpleado');
Route::post('storeEmpleado', [EmpleadoController::class, 'storeEmpleado'])->name('storeEmpleado');
Route::get('empleadosTabla', [EmpleadoController::class, 'empleadosTabla'])->name('empleadosTabla');

//USUARIOS
Route::get('usuarios', [UsuarioController::class, 'index'] )->name('usuarios');
Route::post('store', [UsuarioController::class, 'storeUsuario'])->name('storeUsuario');
