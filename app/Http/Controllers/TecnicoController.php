<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class TecnicoController extends Controller
{
    public function visitasAsignadas(Request $request)
    {
        $tecnico = session('empleado');
        $data = [
            "tecnico_id" => $tecnico['id'],
            "fecha" => $request->fecha,
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/visitasAsignadas', $data);

        $data = $response->json();
        return DataTables::of($data)->make(true);
    }

    public function verVisitaTecnico(Request $request)
    {
        $tecnico = session('empleado');
        $data = [
            "tecnico_id" => $tecnico['id'],
            "id" => $request->id,
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/verVisitaTecnico', $data);

        $data = $response->json();
        $visita = $data['visita'];

        return view('tecnicos.showVisit', compact('visita'));
    }

    public function comenzarVisita(Request $request)
    {
        $tecnico = session('empleado');
        $data = [
            "auth_id" => $tecnico['id'],
            "id" => $request->id,
        ];
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->put(env('API_REST').'/comenzarVisita', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function finalizarVisita(Request $request)
    {
        $tecnico = session('empleado');
        $data = [
            "auth_id" => $tecnico['id'],
            "id" => $request->id,
        ];
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->put(env('API_REST').'/finalizarVisita', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function guardarlog(Request $request)
    {
        $tecnico = session('empleado');
        $data = [
            "auth_id" => $tecnico['id'],
            "comentario" => $request->comentario,
            "id" => $request->id,
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/guardarlog', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function logsTabla(Request $request)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/logsTabla/'.$request->id);

        $data = $response->json();

        return DataTables::of($data)->make(true);
    }

    public function verLocalizacion(Request $request)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/verLocalizacion/'.$request->visita_id);

        $cliente = $response->json();

        return view('tecnicos.verLocalizacion', compact('cliente'));
    }
}
