<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'login']);
    }

    public function login(Request $request)
    {
        $request->validate([
            'usuario' => 'required|email',
            'password' => 'required'
        ]);

        $url = env('API_REST');
        $response = Http::post($url.'/login', [
            "usuario" => $request->usuario,
            "password" => $request->password
        ]);

        $data = $response->json();
        if($data['status'] == 1)
        {
            $user = $data['usuario'];
            $empleado = $data['empleado'];

            session(['usuarioAuth' => $user['usuario']]);
            session(['rol' => $user['rol_id']]);
            session(['access_token' => $data['access_token']]);
            session(['nombreAuth' => $empleado['primer_nombre'].' '.$empleado['primer_apellido']]);
            session(['empleado' => $empleado]);
            return redirect()->route('home');
        }else{
            return back()
            ->withErrors(['usuario' => trans('auth.failed')])
            ->withInput(request(['usuario']));
        }

    }

    public function logout()
    {
        session()->flush();
        return redirect('login');
    }
}
