<?php

namespace App\Http\Controllers\mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UsuarioController extends Controller
{
    public function index()
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/noUsuarios');

        $empleados  = $response->json();

        return view('usuarios.create', compact('empleados'));
    }

    public function storeUsuario(Request $request)
    {
        $data = [
            "usuario" => $request->usuario,
            'password' => $request->pass,
            'password_confirmation' => $request->confirm_pass,
            'rol_id' => $request->rol,
            'employee_id' => $request->empleado
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/register', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }
}
