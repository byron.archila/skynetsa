<?php

namespace App\Http\Controllers\mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class EmpleadoController extends Controller
{
    public function index()
    {
        return view('empleados.index');
    }

    public function createEmpleado()
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/municipios');

        $municipios = $response->json();

        return view('empleados.create', compact('municipios'));
    }

    public function storeEmpleado(Request $request)
    {
        $data = [
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'nacimiento' => $request->nacimiento,
            'dpi' => $request->dpi,
            'dir' => $request->dir,
            'zona' => $request->zona,
            'area' => $request->area,
            'plaza' => $request->plaza,
            'municipio' => $request->municipio
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/guardarEmpleado', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function empleadosTabla()
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/empleadosTabla');

        $data = $response->json();
        return DataTables::of($data)->make(true);
    }
}
