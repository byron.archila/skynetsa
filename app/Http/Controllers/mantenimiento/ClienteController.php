<?php

namespace App\Http\Controllers\mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class ClienteController extends Controller
{
    public function index()
    {
        return view('clientes.index');
    }

    public function createCliente()
    {
        return view('clientes.create');
    }

    public function storeCliente(Request $request)
    {
        $data = [
            'nombre' => $request->nombre,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'latitud' => $request->latitud,
            'longitud' => $request->longitud,
            'descripcion' => $request->descripcion
        ];
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/storeCliente', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function clientesTabla()
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/clientesTabla');

        $data = $response->json();
        return DataTables::of($data)->make(true);
    }

    public function deleteCliente(Request $request)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->delete(env('API_REST').'/deleteCliente/'.$request->id);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function verCliente(Request $request)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/verCliente/'.$request->id);

        $cliente = $response->json();

        return view('clientes.edit', compact('cliente'));
    }

    public function actualizarCliente(Request $request)
    {
        $data = [
            "id" => $request->id,
            "nombre" => $request->nombre,
            "email" => $request->email,
            "descripcion" => $request->descripcion,
            "direccion" => $request->direccion,
            "telefono" => $request->telefono,
            "estado" => $request->estado,
            "longitud" => $request->longitud,
            "latitud" => $request->latitud,
        ];
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->put(env('API_REST').'/actualizarCliente', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }
}
