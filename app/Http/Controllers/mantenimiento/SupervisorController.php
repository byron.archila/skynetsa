<?php

namespace App\Http\Controllers\mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class SupervisorController extends Controller
{
    public function index()
    {
        return view('supervisores.index');
    }

    public function create()
    {
        $supervisor = session('empleado');

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/crearVisita/'.$supervisor['id']);

        $data = $response->json();
        $tecnicos = $data['tecnicos'];
        $clientes = $data['clientes'];
        $estados = $data['estados'];

        return view('supervisores.create', compact('clientes', 'tecnicos', 'estados'));
    }

    public function store(Request $request)
    {
        $supervisor = session('empleado');
        $data = [
            "auth_id" => $supervisor['id'],
            'tecnico_id' => $request->tecnico,
            'cliente_id' => $request->cliente,
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/guardarVisita', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function visitasTabla()
    {
        $supervisor = session('empleado');

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/visitasTabla/'.$supervisor['id']);

        $data = $response->json();
        return DataTables::of($data)->make(true);
    }

    public function deleteVisita(Request $request)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->delete(env('API_REST').'/deleteVisita/'.$request->id);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }

    public function verVisita(Request $request)
    {
        $supervisor = session('empleado');
        $data = [
            "supervisor_id" => $supervisor['id'],
            "id" => $request->id,
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->post(env('API_REST').'/verVisita', $data);

        $data = $response->json();
        $tecnicos = $data['tecnicos'];
        $clientes = $data['clientes'];
        $visita = $data['visita'];

        return view('supervisores.edit', compact('tecnicos', 'clientes', 'visita'));
    }

    public function verVisitaSupervisor(Request $request)
    {
        $data = [
            "id" => $request->id,
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->get(env('API_REST').'/verVisitaTecnico', $data);

        $data = $response->json();
        $visita = $data['visita'];

        return view('supervisores.showVisita', compact('visita'));
    }

    public function actualizarVisita(Request $request)
    {
        $data = [
            "id" => $request->id,
            'tecnico_id' => $request->tecnico,
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha
        ];

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ])->put(env('API_REST').'/actualizarVisita', $data);

        if($response->json()['status'] == 1){
            return 'success';
        }else{
            return 'error';
        }
    }
}
